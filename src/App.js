import React from "react";
import "./styles/App.css";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import Home from "./components/Home.js";

const theme = createMuiTheme({
  palette: {
    type: "light",
  },
});

function App() {
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <Home />
      </ThemeProvider>
    </div>
  );
}

export default App;
