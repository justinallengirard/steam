class SteamAPI {
  constructor() {
    this.AWS_API_GATEWAY_URL = "https://4ug0t0xfc8.execute-api.us-east-2.amazonaws.com/dev";
    // this.STEAM_API_GAME_DETAILS_URL = "https://store.steampowered.com/api/appdetails";
    this.headers = {
      "Content-Type": "application/json",
    };
    this.mode = "cors";
    this.credentials = "omit";
    this.httpGET = "GET";
  }

  getGameDetails(gameID, callback) {
    // let url = new URL(this.AWS_API_GATEWAY_URL);
    // url.searchParams.append("function", "GET_GAMES_LIST");
    // url.searchParams.append("gameIDs", );
    // this.fetchGET(url, (successful, s3) => {
    //   if (successful) {
    //     this.fetchGamesFile(s3, (fileRetrieved, games) => {
    //       if (fileRetrieved) {
    //         callback(true, games);
    //       } else {
    //         callback(false, []);
    //       }
    //     });
    //   } else {
    //     callback(false, []);
    //   }
    // });
  }

  getGameList(callback) {
    let url = new URL(this.AWS_API_GATEWAY_URL);
    url.searchParams.append("function", "GET_GAMES_LIST");
    this.fetchGET(url, (successful, s3) => {
      if (successful) {
        this.fetchGamesFile(s3, (fileRetrieved, games) => {
          if (fileRetrieved) {
            callback(true, games);
          } else {
            callback(false, []);
          }
        });
      } else {
        callback(false, []);
      }
    });
  }

  fetchGamesFile(url, callback) {
    fetch(url)
      .then((response) => response.text())
      .then((text) => JSON.parse(text))
      .then(
        (games) => {
          callback(true, games);
        },
        (error) => {
          callback(false, error);
        }
      );
  }

  fetchGET(url, callback) {
    fetch(url, {
      method: this.httpGET,
      mode: this.mode,
      credentials: this.credentials,
      body: null,
      headers: this.headers,
    })
      .then((response) => response.json())
      .then(
        (result) => {
          if (result.successful) {
            callback(true, result.data);
          } else {
            callback(false, "");
          }
        },
        (error) => {
          callback(false, error);
        }
      );
  }
}

export default SteamAPI;
