import React from "react";
import SteamAPI from "../SteamAPI.js";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import CircularProgress from "@material-ui/core/CircularProgress";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";

class GameList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      games: [],
      lastIndex: 0,
      startIndex: 0,
      endIndex: props.pageSize - 1,
      pageSize: props.pageSize,
      page: [],
      failedToRetrieveGames: false,
    };

    this.SteamAPI = new SteamAPI();
  }

  componentDidMount() {
    this.setState(
      { loading: true },
      this.SteamAPI.getGameList((successful, games) => {
        if (successful) {
          let page = [];
          if (games.length < this.state.pageSize) {
            page = games;
          } else {
            page = games.slice(this.state.startIndex, this.state.endIndex);
          }
          this.setState({ loading: false, games: games, page: page, lastIndex: games.length - 1 });
        } else {
          this.setState({ loading: false, showFailure: true });
        }
      })
    );
  }

  getPreviousPage() {
    if (this.state.games.length < this.state.pageSize) {
      this.setState({ page: this.state.games });
    } else {
      let start = 0;
      let end = 0;
      if (this.state.startIndex - this.state.pageSize < 0) {
        start = 0;
        end = this.state.pageSize - 1;
      } else {
        start = this.state.startIndex - this.state.pageSize;
        end = this.state.endIndex - this.state.pageSize;
      }
      this.setState({ page: this.state.games.slice(start, end), startIndex: start, endIndex: end });
    }
  }

  getNextPage() {
    if (this.state.games.length < this.state.pageSize) {
      this.setState({ page: this.state.games });
    } else {
      let start = 0;
      let end = 0;
      if (this.state.endIndex + this.state.pageSize > this.state.lastIndex) {
        let diff = this.state.lastIndex - this.state.endIndex;
        start = this.state.start + diff;
        end = this.state.endIndex + diff;
      } else {
        start = this.state.startIndex + this.state.pageSize;
        end = this.state.endIndex + this.state.pageSize;
      }

      this.setState({ page: this.state.games.slice(start, end), startIndex: start, endIndex: end });
    }
  }

  viewDetails(gameID) {
    window.open("https://store.steampowered.com/app/" + gameID, "_blank");
  }

  render() {
    if (this.state.loading) {
      return (
        <Grid container direction="column" justify="flex-start" alignItems="center">
          <Grid item xs>
            <CircularProgress style={{ color: "black" }} />
          </Grid>
        </Grid>
      );
    } else if (this.state.failedToRetrieveGames) {
      return (
        <Grid container direction="column" justify="flex-start" alignItems="center">
          <Grid item xs>
            <Typography variant="h4" align="center">
              Failed to retrieve games!
            </Typography>
          </Grid>
        </Grid>
      );
    } else {
      return (
        <Grid container direction="column" justify="flex-start" alignItems="flex-start">
          <Grid item xs style={{ alignSelf: "stretch" }}>
            <List>
              {this.state.page.map((game) => (
                <ListItem key={game.gameID} style={{ width: "100%" }}>
                  <Grid container direction="row" justify="space-between" alignItems="flex-start">
                    <Grid item xs style={{ flexGrow: 1 }}>
                      <ListItemText primary={game.title} />
                    </Grid>
                    <Grid item xs style={{ flexGrow: 0, alignSelf: "center" }}>
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={() => {
                          this.viewDetails(game.gameID);
                        }}
                        style={{ height: "100%", width: 200 }}
                      >
                        View Details
                      </Button>
                    </Grid>
                  </Grid>
                </ListItem>
              ))}
            </List>
          </Grid>
          <Grid item xs style={{ width: "100%" }}>
            <Grid container direction="row" justify="flex-end" alignItems="flex-end" style={{ marginTop: 30 }}>
              <Grid item xs style={{ flexGrow: 0 }}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => {
                    this.getPreviousPage();
                  }}
                  style={{ width: 200, height: 70, margin: 10 }}
                  disabled={this.state.startIndex === 0}
                >
                  Previous Page
                </Button>
              </Grid>
              <Grid item xs style={{ flexGrow: 0 }}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => {
                    this.getNextPage();
                  }}
                  style={{ width: 200, height: 70, margin: 10 }}
                  disabled={this.state.endIndex === this.state.lastIndex}
                >
                  Next Page
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      );
    }
  }
}

export default GameList;
