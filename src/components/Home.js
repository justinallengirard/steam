import React from "react";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import GameList from "./GameList.js";
import SteamIcon from "../images/steam.png";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Grid container direction="column" justify="flex-start" alignItems="center">
        <Grid item xs>
          <Grid container direction="column" justify="flex-start" alignItems="center" style={{ marginBottom: 50 }}>
            <Grid item xs>
              <img src={SteamIcon} alt="steam" height={100} />
            </Grid>
            <Grid item xs>
              <Typography variant="h2" align="center">
                Steam Game Library
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs>
          <Grid container direction="row" justify="flex-start" alignItems="flex-start">
            <Grid item xs style={{ margin: 20 }}>
              <Paper elevation={3} style={{ padding: 20, width: "50vw" }}>
                <GameList pageSize={10} />
              </Paper>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default Home;
